FROM python:3.9

WORKDIR /app

RUN pip install psycopg2

COPY main.py /app/main.py

ENTRYPOINT ["python3"]
CMD ["/app/main.py"]
