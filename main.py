import psycopg2

conn = psycopg2.connect("dbname=lab host=postgres password=password user=user")

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
buy_query_inventory_total = "SELECT sum(amount) FROM Inventory WHERE username = %(username)s"
buy_update_inventory = "UPDATE Inventory SET amount = amount + %(amount)s WHERE username = %(username)s AND product = %(product)s"

def buy_product(username, product, amount):
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Not enough funds on balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            try:
                cur.execute(buy_query_inventory_total, obj)
                total = cur.fetchone()[0]
                if total + amount > 100:
                    raise Exception("Total cannot be more than 100")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Cannot update inventory")

            cur.execute(buy_update_inventory, obj)
            conn.commit()

buy_product("Alice", "marshmello", 20)
